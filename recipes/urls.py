from django.urls import path
from recipes.models import Recipe

from recipes.views import (
    RecipeUpdateView,
    RecipeDeleteView,
    log_rating,
    RecipeListView,
    RecipeDetailView,
    RecipeCreateView,
)


urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
]
